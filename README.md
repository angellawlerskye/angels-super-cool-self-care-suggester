# Angel's Super Cool Self-Care Suggester

## Description
A super cool and super simple front-end project I created to practice HTML, CSS, and JavaScript.

## Usage
Download the file called angels-super-cool-self-care-suggester and open it in your browser.
Enter your name into the form and click "Get Task" to generate random simple self-care tasks you can do to improve your day.

![Alt text](image.png)